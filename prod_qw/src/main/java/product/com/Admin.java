package product.com;

public class Admin {
	private String admin_username;
	private String admin_email;
	private String admin_password;
	
	public Admin() {
		super();
	}
	
	public Admin(String admin_username, String admin_password) {
		super();
		this.admin_username = admin_username;
		this.admin_password = admin_password;
	}

	public Admin(String admin_username, String admin_email, String admin_password) {
		super();
		this.admin_username = admin_username;
		this.admin_email = admin_email;
		this.admin_password = admin_password;
	}

	public String getAdmin_username() {
		return admin_username;
	}

	public void setAdmin_username(String admin_username) {
		this.admin_username = admin_username;
	}

	public String getAdmin_email() {
		return admin_email;
	}

	public void setAdmin_email(String admin_email) {
		this.admin_email = admin_email;
	}

	public String getAdmin_password() {
		return admin_password;
	}

	public void setAdmin_password(String admin_password) {
		this.admin_password = admin_password;
	}
}
