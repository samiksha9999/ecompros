package product.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import product.com.Admin;
import product.dao.AdminDao;

/**
 * Servlet implementation class LoginAdmin
 */
public class LoginAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginAdmin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    	response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "*");
		response.addHeader("Access-Control-Allow-Headers", "*");
		
    	AdminDao dao = new AdminDao();
    	PrintWriter pw = response.getWriter();
    	String status="";
    	
    	String username = request.getParameter("admin_username");
    	String password = request.getParameter("admin_password");
//    	String email = request.getParameter("email");
    	Admin rec=null;
    	
    	try {
    		rec =dao.login(username, password);
    		if (rec != null) {
    			status = "Succesful";
    		}else {
    			status = "Failed";
    		}
    		}catch (Exception e) {
    			e.printStackTrace();
    			}
    	response.setContentType("application/json");
    	response.setCharacterEncoding("UTF-8");
    	Gson gson = new Gson();
    	String ajson = gson.toJson(status);
    	pw.print(ajson);
    	pw.flush();
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
