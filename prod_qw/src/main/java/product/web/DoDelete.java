package product.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

//import product.com.Ecommerce;
import product.dao.EcommerceDao;

/**
 * Servlet implementation class DoDelete
 */
public class DoDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DoDelete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "*");
		response.addHeader("Access-Control-Allow-Headers", "*");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "*");
		response.addHeader("Access-Control-Allow-Headers", "http://localhost:3000/product");
	}
	
	/**
	 * @see HttpServlet#doDelete(HttpServletRequest request, HttpServletResponse response)
	 */
	
	public void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "*");
		response.addHeader("Access-Control-Allow-Headers", "*");

		EcommerceDao dao = new EcommerceDao();
		PrintWriter out = response.getWriter();
		int id = Integer.parseInt(request.getParameter("product_id"));
//		System.out.println(id);
//		Ecommerce e = null;
		int status=0;
		int rec = 0;
		try {
			rec = dao.delete(id);
			} catch (Exception b) {
				b.printStackTrace();
				}
		if (rec > 0) {
			status=200;
		}else {
			status = 404;
			}
		String productJsonString = new Gson().toJson(status);
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.write(productJsonString);
		out.close();
		}
}

