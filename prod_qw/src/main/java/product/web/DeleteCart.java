package product.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import product.dao.CartDao;

/**
 * Servlet implementation class DeleteCart
 */
public class DeleteCart extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteCart() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "*");
		response.addHeader("Access-Control-Allow-Headers", "http://localhost:3000/usercart");

		CartDao dao = new CartDao();
		PrintWriter out = response.getWriter();
		int id = Integer.parseInt(request.getParameter("id"));
		
		String status="";
		int rec = 0;
		try {
			rec = dao.delete(id);
			} catch (Exception b) {
				b.printStackTrace();
				}
		if (rec > 0) {
			status= "Sucessfull";
		}else {
			status ="Failed to Delete";
			}
		String productJsonString = new Gson().toJson(status);
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.write(productJsonString);
		out.close();
	}

}
