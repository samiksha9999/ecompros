package product.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import product.com.Users;
import product.dao.UserDao;

/**
 * Servlet implementation class Login
 */
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }
    public void init(ServletConfig config) throws ServletException {}
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "*");
		response.addHeader("Access-Control-Allow-Headers", "*");
		
    	UserDao dao = new UserDao();
    	PrintWriter pw = response.getWriter();
    	String status="";
    	
    	String username = request.getParameter("username");
    	String password = request.getParameter("password");
//    	String email = request.getParameter("email");
    	Users rec=null;
    	
    	try {
    		rec =dao.login(username, password);
    		if (rec != null) {
    			status = "Succesful";
    		}else {
    			status = "Failed";
    		}
    		}catch (Exception e) {
    			e.printStackTrace();
    			}
    	response.setContentType("application/json");
    	response.setCharacterEncoding("UTF-8");
    	Gson gson = new Gson();
    	String ajson = gson.toJson(status);
    	pw.print(ajson);
    	pw.flush();
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	doGet(request, response);
    }
}