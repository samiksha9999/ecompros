package product.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import product.com.Cart;
import product.dao.CartDao;


public class AddCart extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "*");
		response.addHeader("Access-Control-Allow-Headers", "http://localhost:3000/usercart");
		
		List<Cart> cart = new LinkedList<>();
		CartDao dao = new CartDao();
		
		cart = dao.getAllCart();
		
		Gson gson = new Gson();
		String cartjson= gson.toJson(cart);
		
		PrintWriter pw = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		pw.write(cartjson);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "*");
		response.addHeader("Access-Control-Allow-Headers", "http://localhost:3000/userproduct");
		
		CartDao dao =new CartDao();
		PrintWriter p =response.getWriter();
		
		int id = Integer.parseInt (request.getParameter("id"));
		String name=request.getParameter("name");
		String decs=request.getParameter("desc");
		int quantity = Integer.parseInt(request.getParameter("quantity"));
		int price = Integer.parseInt(request.getParameter("price"));
		String image=request.getParameter("pimage");
		
		Cart cart = new Cart(id,name,decs,quantity,price,image);

		String insert="";
		try {
			int record=dao.insertcart(cart);
			p.print(record);
			if(record>0){
				insert="Sucessfull";
				}
			} catch (Exception c) {
				c.printStackTrace();
				insert="Failed to Insert";
				}
			String productJsonString=new Gson().toJson(insert);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			p.print(productJsonString);
			p.flush();
	}
	
}


