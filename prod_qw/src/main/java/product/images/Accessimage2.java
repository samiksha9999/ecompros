package product.images;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.File;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.google.gson.Gson;

@WebServlet("/api/images")
public class Accessimage2 extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "*");
		response.addHeader("Access-Control-Allow-Headers", "http://localhost:3000/usercart");

    	String accessKey = "AKIAYBZS7QDXWLHDRCMV";
		String secretKey = "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";

		AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
		AmazonS3 s3client = AmazonS3ClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(credentials))
				.withRegion(Regions.AP_SOUTH_1)
				.build();
    	
        // Initialize the Amazon S3 client
    //    AmazonS3 s3client = AmazonS3ClientBuilder.standard().withRegion(Regions.AP_SOUTH_1).build();

        // Get the list of object keys from your S3 bucket
        ListObjectsV2Result result = s3client.listObjectsV2("bucketeco");
        List<S3ObjectSummary> objects = result.getObjectSummaries();
        List<String> imageUrls = new ArrayList<>();

        // Loop through the list of object keys and generate the pre-signed URL for each image
        for (S3ObjectSummary object : objects) {
            String imageUrl = s3client.generatePresignedUrl("bucketeco", object.getKey(), Date.from(Instant.now().plus(Duration.ofMinutes(5)))).toString();
            imageUrls.add(imageUrl);
        }

        // Set the response type to JSON and send the list of image URLs back to the client
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.print(new Gson().toJson(imageUrls));
        out.flush();
    }
}
