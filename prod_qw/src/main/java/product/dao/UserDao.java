package product.dao;

import java.sql.*;
import product.com.Users;

public class UserDao {
	public int signup(Users us) throws ClassNotFoundException {
		int record = 0;
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:330/prod", "root", "root");

			PreparedStatement ps = con.prepareStatement("insert into user values(?,?,?,?)");
			ps.setString(1, us.getUserName());
			ps.setString(2, us.getEmail());
			ps.setString(3, us.getPassword());
			ps.setInt(4,1);
			record = ps.executeUpdate();
			ps.close();
			return record;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return record;
	}

	public Users login(String username,String password) throws ClassNotFoundException {
		Users user=null;
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mariadb://localhost:3308/prod", "root", "root");
			PreparedStatement ps = con.prepareStatement("select * from user where username=? and password=? and role_id=1");
			 ps.setString(1, username);
			 ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			
			 if (rs.next()==true){
				 user=new Users(rs.getString(1),rs.getString(3));
				 System.out.println("Valid user, Login Successful!");
			} else if(rs.next()==false){
				System.out.println("Invalid User, kindly recheck your username or password.");
				
				}
			rs.close();
			ps.close();
			return user;
			} catch (SQLException e){
				e.printStackTrace();
				}
		return user;
		}
}
